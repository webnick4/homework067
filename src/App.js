import React, { Component } from 'react';
import DigitalLock from "./containers/DigitalLock/DigitalLock";


class App extends Component {
  render() {
    return <DigitalLock />;
  }
}

export default App;
