import React, { Component } from 'react';
import './DigitalLock.css';
import { connect } from 'react-redux';

class DigitalLock extends Component {

  showPinCode() {
    const pinCode = this.props.enterCode;
    return Array(pinCode.length + 1).join('*');
  }

  render() {
    return (
      <div className={`DigitalLock ${ this.props.statusCode ? 'DigitalLock-bd_green' : '' }`}>
        <div className="DigitalLock-monitor">{this.showPinCode()}</div>
        <div className="DigitalLock-panel">
          {Object.keys(this.props.keyboard).map(codeValue => (
            <button
              key={codeValue}
              className={`DigitalLock-btn
                          ${ this.props.keyboard[codeValue] === '<' ? 'DigitalLock-btn_red' : '' }
                          ${ this.props.keyboard[codeValue] === 'E' ? 'DigitalLock-btn_green' : '' }
                         `}
              onClick={() => this.props.addCode(codeValue)}
            >
              {this.props.keyboard[codeValue]}
            </button>
          ))}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    keyboard: state.keyboard,
    enterCode: state.enterCode,
    statusCode: state.statusCode
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addCode: (value) => dispatch({type: 'ADD_CODE', value})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DigitalLock);