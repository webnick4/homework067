import { PIN_CODE } from '../constants';

const initialState = {
  keyboard: {
    seven: 7,
    eight: 8,
    nine: 9,
    four: 4,
    five: 5,
    six: 6,
    one: 1,
    two: 2,
    three: 3,
    decrement: '<',
    zero: 0,
    enter: 'E'
  },
  enterCode: [],
  statusCode: false
};


const reducer = (state = initialState, action) => {
  if (action.type === 'ADD_CODE') {
    let counterNumber = state.enterCode.length;

    if (action.value !== 'decrement' && action.value !== 'enter' && counterNumber <= 3) {
      let code = state.enterCode;
      code.push(state.keyboard[action.value]);
    }

    if (action.value === 'decrement') {
      let code = state.enterCode;
      code.splice(-1, 1);
    }

    if (action.value === 'enter') {
      const code = state.enterCode;
      const getPinCode = code.join('');

      if (getPinCode !== PIN_CODE) {
        // console.log(PIN_CODE + ' ' + code.join(''));
        alert('Pin Code Wrong!');
        state.enterCode = [];
      } else {
        state.statusCode = true;
        alert('Access Granted');
      }
    }

     return {
       ...state,
       enterCode: [...state.enterCode],
       statusCode: state.statusCode
    };
  }
  return state;
};

export default reducer;